locals {
  tags = {
    environment = "handson"
    source = "terraform"
    deployed_by = "rackspace-terraform"
  }
}

# Se crea el resource group
resource "azurerm_resource_group" "handson_resource_group" {
  name     = var.resource_group_name
  location = var.location
  tags     = local.tags
}
