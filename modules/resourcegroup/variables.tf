variable "resource_group_name" { 
  description = "The name of the resource group"
}

variable "location" {
  description = "The region where the resource it's going to be deployed"
  default = "East Us"   
}