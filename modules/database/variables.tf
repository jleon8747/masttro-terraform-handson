variable "resource_group_name" { 
  description = "The name of the resource group"
}

variable "location" {
  description = "The region where the resource it's going to be deployed"
  default = "East Us"   
}

variable "prefix" {
  description = "The prefix that we are going to use on the vm"
}

variable "virtual_network_name" {
  description = "The virtual network name for the sql managed instance subnet"
}