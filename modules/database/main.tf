locals {
  tags = {
    environment = "handson"
    source = "terraform"
    deployed_by = "rackspace-terraform"
  }
}

# Create security group
resource "azurerm_network_security_group" "main_sg" {
    name                = "${var.prefix}-nsg"
    location            = var.location
    resource_group_name = var.resource_group_name
    tags                = local.tags
}

# Creates the database subnet
resource "azurerm_subnet" "internal" {
    name                 = "${var.prefix}-db-subnet"
    resource_group_name  = var.resource_group_name
    virtual_network_name = var.virtual_network_name
    address_prefixes     = ["10.0.3.0/24"]

    delegation {
        name = "managedinstancedelegation"

        service_delegation {
            name    = "Microsoft.Sql/managedInstances"
            actions = [
                "Microsoft.Network/virtualNetworks/subnets/join/action",
                "Microsoft.Network/virtualNetworks/subnets/prepareNetworkPolicies/action",
                "Microsoft.Network/virtualNetworks/subnets/unprepareNetworkPolicies/action"
            ]
        }
    }
}

# Associates subnet and the security group
resource "azurerm_subnet_network_security_group_association" "main" {
    subnet_id                 = azurerm_subnet.internal.id
    network_security_group_id = azurerm_network_security_group.main_sg.id
}

# Create route table
resource "azurerm_route_table" "main" {
    name                          = "${var.prefix}-rt"
    location                      = var.location
    resource_group_name           = var.resource_group_name
    disable_bgp_route_propagation = false
    tags                          = local.tags
}

# Associate subnet and the route table
resource "azurerm_subnet_route_table_association" "main" {
    subnet_id      = azurerm_subnet.internal.id
    route_table_id = azurerm_route_table.main.id
}

# Se crea la managed instance
resource "azurerm_mssql_managed_instance" "main" {
    name                         = "${var.prefix}-mssql"
    resource_group_name          = var.resource_group_name
    location                     = var.location
    subnet_id                    = azurerm_subnet.internal.id
    administrator_login          = "masttro-admin"
    administrator_login_password = "mastt$r%o-handson1234"
    license_type                 = "BasePrice"
    sku_name                     = "GP_Gen5"
    vcores                       = 8
    storage_size_in_gb           = 32
    tags                         = local.tags
}