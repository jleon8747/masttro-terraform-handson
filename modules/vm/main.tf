locals {
  tags = {
    environment = "handson"
    source = "terraform"
    deployed_by = "rackspace-terraform"
  }
}

# Create virtual network
resource "azurerm_virtual_network" "main" {
  name                = "${var.prefix}-network"
  address_space       = ["10.0.0.0/16"]
  location            = var.location
  resource_group_name = var.resource_group_name
  tags                = local.tags
}

# Create vm subnet
resource "azurerm_subnet" "internal" {
  name                 = "${var.prefix}-web-subnet"
  resource_group_name  = var.resource_group_name
  virtual_network_name = azurerm_virtual_network.main.name
  address_prefixes     = ["10.0.2.0/24"]
}

# Create public ip address
resource "azurerm_public_ip" "public_ip" {
  name                = "${var.prefix}-vm-ip"
  resource_group_name = var.resource_group_name
  location            = var.location
  allocation_method   = "Dynamic"
  tags                = local.tags
}

# Create vm network interface
resource "azurerm_network_interface" "main" {
  name                = "${var.prefix}-nic"
  resource_group_name = var.resource_group_name
  location            = var.location

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.internal.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id = azurerm_public_ip.public_ip.id
  }
}

# Add security group for ssh access
resource "azurerm_network_security_group" "nsg" {
  name                = "ssh_nsg"
  location            = var.location
  resource_group_name = var.resource_group_name
  tags                = local.tags

  security_rule {
    name                       = "allow_ssh_sg"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

# Associate security group to network interface
resource "azurerm_network_interface_security_group_association" "association" {
  network_interface_id      = azurerm_network_interface.main.id
  network_security_group_id = azurerm_network_security_group.nsg.id
}

# Create virtual machine
resource "azurerm_linux_virtual_machine" "main" {
  name                            = "${var.prefix}-vm"
  resource_group_name             = var.resource_group_name
  location                        = var.location
  size                            = "Standard_F2"
  admin_username                  = "adminuser"
  tags                            = local.tags  

  # Attach the network interface
  network_interface_ids = [
    azurerm_network_interface.main.id,
  ]

  # Add the user ssh key for the admin user
  admin_ssh_key {
    username = "adminuser"
    public_key = file("~/.ssh/id_rsa.pub")
  }

  # Sets the image to create the virtual machine
  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  # Attach disk for the os
  os_disk {
    storage_account_type = "Standard_LRS"
    caching              = "ReadWrite"
  }
}

# Virtual Machine Backup Enablement
# Se crea el vault para guardar los backups
resource "azurerm_recovery_services_vault" "main" {
  name                = "${var.prefix}-backup-vault"
  location            = var.location
  resource_group_name = var.resource_group_name
  sku                 = "Standard"
  storage_mode_type   = "LocallyRedundant"
  depends_on          = [azurerm_linux_virtual_machine.main]
  tags                = local.tags
}

# Se crea la politica para hacer el backup
resource "azurerm_backup_policy_vm" "main" {
  name                = "${var.prefix}-backup-policy"
  resource_group_name = var.resource_group_name
  recovery_vault_name = azurerm_recovery_services_vault.main.name
  policy_type         = "V2"
  timezone            = "Central Standard Time (Mexico)"

  backup {
    frequency = "Daily"
    time      = "23:00"
  }

  retention_daily {
    count = 7
  }

  retention_weekly {
    count = 4
    weekdays = ["Sunday"]
  }

  depends_on          = [azurerm_linux_virtual_machine.main]
}

# Se enciende el servicio de backup
resource "azurerm_backup_protected_vm" "testvm" {
  resource_group_name = var.resource_group_name
  recovery_vault_name = azurerm_recovery_services_vault.main.name
  source_vm_id        = azurerm_linux_virtual_machine.main.id
  backup_policy_id    = azurerm_backup_policy_vm.main.id
  depends_on          = [azurerm_linux_virtual_machine.main]
  tags                = local.tags
  
  timeouts {
    create = "120m"
    delete = "5h"
    update = "90m"
    read   = "10m" 
  }
}