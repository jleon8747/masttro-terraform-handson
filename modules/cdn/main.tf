locals {
  tags = {
    environment = "handson"
    source = "terraform"
    deployed_by = "rackspace-terraform"
  }
}

# Se genera el storage account para el servicio de cdn
resource "azurerm_storage_account" "cdn_storage" {
    name                     = "masttrohandsonstorage"
    location                 = var.location
    resource_group_name      = var.resource_group_name
    account_tier             = "Standard"
    account_replication_type = "LRS"
    tags                     = local.tags
}

# Se crea el perfil para la cdn
resource "azurerm_cdn_profile" "cdn_profile" {
    name                = "${var.prefix}-cdn"
    location            = var.location
    resource_group_name = var.resource_group_name
    sku                 = "Standard_Microsoft"
    tags                = local.tags
}

# Se genera un endpoint para que la cdn sea accesible
resource "azurerm_cdn_endpoint" "cdn_endpoint" {
    name                = "${var.prefix}-cdn"
    profile_name        = "${azurerm_cdn_profile.cdn_profile.name}"
    location            = var.location
    resource_group_name = var.resource_group_name
    tags                = local.tags

    origin {
        name       = "${var.prefix}origin1"
        host_name  = "example.com"
        http_port  = 80
        https_port = 443
    }
}