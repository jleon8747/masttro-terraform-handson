# terraform {
#   required_providers {
#     azurerm = {
#       source = "hashicorp/azurerm"
#       version = "=3.0.1"
#     }
#   }
# }

provider "azurerm" {
  features {} 
}

# provider "azurerm" {
#   subscription_id = "..."
#   client_id       = "..."
#   client_secret   = "..."
#   tenant_id       = "..."
# }

module "resourcegroup" {
  source              = "./modules/resourcegroup"

  resource_group_name = var.resource_group_name
  location            = var.location
}

module "virtualmachine" {
  source = "./modules/vm"

  resource_group_name = var.resource_group_name
  location            = var.location
  prefix              = var.prefix
  depends_on          = [module.resourcegroup.rg_name]
}

module "cdn" {
  source = "./modules/cdn"

  resource_group_name = var.resource_group_name
  location            = var.location
  prefix              = var.prefix
  depends_on          = [module.resourcegroup.rg_name]
}

module "database" {
  source = "./modules/database"

  resource_group_name  = var.resource_group_name
  location             = var.location
  prefix               = var.prefix
  virtual_network_name = module.virtualmachine.network_name
  depends_on          = [module.virtualmachine.network_name]
}

